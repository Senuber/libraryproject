FROM openjdk:11
EXPOSE 8080
ADD target/senuberhw3-0.0.1-SNAPSHOT.jar senuberhw3-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","senuberhw3-0.0.1-SNAPSHOT.jar"]