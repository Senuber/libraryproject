package edu.ada.senuberhw3.repository;

import edu.ada.senuberhw3.model.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findAllByName(String name);
    List<Book> findAllByCategory(String category);
    List<Book> findAllByAuthor(String author);
    List<Book> findAllByNameAndCategory(String name, String category);
    List<Book> findAllByNameAndAuthor(String name, String author);
    List<Book> findAllByCategoryAndAuthor(String category, String author);
    List<Book> findAllByNameAndCategoryAndAuthor(String name, String category, String author);
}
