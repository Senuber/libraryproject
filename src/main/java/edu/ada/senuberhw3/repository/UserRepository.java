package edu.ada.senuberhw3.repository;

import edu.ada.senuberhw3.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>
{
    User findFirstByEmailAndPassword(String email, String password);
    User findFirstByEmail(String email);
    User findFirstByToken(String token);
}