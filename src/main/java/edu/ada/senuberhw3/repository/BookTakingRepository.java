package edu.ada.senuberhw3.repository;

import edu.ada.senuberhw3.model.Book;
import edu.ada.senuberhw3.model.BookTaking;
import edu.ada.senuberhw3.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookTakingRepository extends CrudRepository<BookTaking, Long>
{
	List<BookTaking> findAllByUser(User user);
	BookTaking findFirstByBookAndDoneFalse(Book book);
}
