package edu.ada.senuberhw3.repository;

import edu.ada.senuberhw3.model.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends MongoRepository<Comment, String>
{
	Optional<List<Comment>> findAllByBookExtId(String bookId);
}
