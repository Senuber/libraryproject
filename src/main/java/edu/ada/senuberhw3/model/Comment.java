package edu.ada.senuberhw3.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Document
public class Comment
{
	@Id
	private String id;
	private String bookExtId;
	private String commentAuthorName;
	private String commentContent;
	
	@OneToMany(mappedBy = "replies")
	private List<Comment> replies = new ArrayList<>(1);
	
	public Comment(String bookExtId, String commentAuthorName, String commentContent)
	{
		this.bookExtId = bookExtId;
		this.commentAuthorName = commentAuthorName;
		this.commentContent = commentContent;
	}
}
