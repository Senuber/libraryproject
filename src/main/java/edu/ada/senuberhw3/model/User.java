package edu.ada.senuberhw3.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Table(name = "users")
@ToString
@Setter
@Getter
@Entity
public class User
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String lastName;

    private String birthday;

    private String email;
    
    @JsonIgnore
    private String password;
    
    @JsonIgnore
    private String token;
    
    @OneToMany(targetEntity = BookTaking.class, mappedBy = "user", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<BookTaking> history;
    
    public User()
    {
    }

    public User(RegistrationModel registrationModel)
    {
        this.firstName = registrationModel.getFirstName();
        this.lastName = registrationModel.getLastName();
        this.birthday = registrationModel.getBirthday();
        this.email = registrationModel.getEmail();
        this.password = registrationModel.getPassword();
    }
    
    public String concatFirstAndLastName()
    {
        return firstName + " " + lastName;
    }
}