package edu.ada.senuberhw3.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "books")
public class Book {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String category;
    private String author;
    private String pubDate;
    private Long takerId;
    
    @JsonIgnore()
    @OneToMany(targetEntity = BookTaking.class, mappedBy = "book", cascade = CascadeType.ALL)
    private List<BookTaking> history;
    
    
    public Book(String name, String category, String author, String pubDate) {
        this.name = name;
        this.category = category;
        this.author = author;
        this.pubDate = pubDate;
    }
    
    /**
     * Returns the User that has taken this book.
     * Returns null if the book is available (taken by no-one)
     * This function is meant to be called automatically upon conversion to JSON.
     * @return
     */
    public User getCurrentTaker()
    {
        for(BookTaking bookTaking: history)
        {
            if (!bookTaking.isDone())
            {
                return bookTaking.getUser();
            }
        }
        
        return null;
    }
}
