package edu.ada.senuberhw3.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
public class CommentModel implements Serializable
{
	private String id;
	private String commentAuthorName;
	private String commentContent;
	
	private List<CommentModel> replies;
	
	public CommentModel(Comment comment)
	{
		this.id = comment.getId();
		this.commentAuthorName = comment.getCommentAuthorName();
		this.commentContent = comment.getCommentContent();
		
		populateRepliesFromCommentList(comment.getReplies());
	}
	
	private void populateRepliesFromCommentList(List<Comment> commentList)
	{
		this.replies = new ArrayList<>(1);
		
		if (commentList != null && !commentList.isEmpty())
		{
			commentList.stream().forEach(comment -> this.replies.add(new CommentModel(comment)));
		}
	}
}
