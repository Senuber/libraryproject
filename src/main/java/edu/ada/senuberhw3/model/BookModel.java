package edu.ada.senuberhw3.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class BookModel implements Serializable
{
	private Long id;
	private String name;
	private String category;
	private String author;
	private String pubDate;
	private Long takerId;
	private List<CommentModel> comments;
	
	public BookModel(Book book)
	{
		this.id = book.getId();
		this.name = book.getName();
		this.category = book.getCategory();
		this.author = book.getAuthor();
		this.pubDate = book.getPubDate();
		this.takerId = book.getTakerId();
	}
}
