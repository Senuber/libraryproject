package edu.ada.senuberhw3.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "book_takings")
@ToString
@Setter
@Getter
@Entity
public class BookTaking
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(targetEntity = User.class) @JoinColumn()
	private User user;
	
	@ManyToOne(targetEntity = Book.class) @JoinColumn()
	private Book book;
	
	private boolean done;
	
	public BookTaking()
	{
	}
	
	public BookTaking(User user, Book book)
	{
		this.user = user;
		this.book = book;
	}
}
