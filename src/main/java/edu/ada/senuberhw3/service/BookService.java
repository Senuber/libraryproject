package edu.ada.senuberhw3.service;

import edu.ada.senuberhw3.model.Book;
import edu.ada.senuberhw3.model.BookModel;
import edu.ada.senuberhw3.model.User;

import java.util.List;

public interface BookService {
    List<Book> getAll();
    Book getById(Long id);
    BookModel getBookModelById(Long id);
    List<Book> getByName(String name);
    List<Book> getByCategory(String category);
    List<Book> getByAuthor(String author);
    List<Book> getByNameAndCategory(String name, String category);
    List<Book> getByNameAndAuthor(String name, String author);
    List<Book> getByCategoryAndAuthor(String category, String author);
    List<Book> getByNameAndCategoryAndAuthor(String name, String category, String author);
    boolean takeBook(User user, Book book);
    boolean returnBook(User user, Book book);
}
