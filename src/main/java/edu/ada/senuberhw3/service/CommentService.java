package edu.ada.senuberhw3.service;

import edu.ada.senuberhw3.model.CommentModel;

import java.util.List;

public interface CommentService
{
	void createComment(String commentAuthorName, Long bookID, String commentContent);
	boolean createReply(String commentAuthorName, String commentID, String commentContent);
	List<CommentModel> getCommentByBookExtId(Long bookID);
}
