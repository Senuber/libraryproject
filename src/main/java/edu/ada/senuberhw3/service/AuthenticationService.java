package edu.ada.senuberhw3.service;

import edu.ada.senuberhw3.model.RegistrationModel;
import edu.ada.senuberhw3.model.User;

public interface AuthenticationService {

    User registration(RegistrationModel registrationModel);

    User login(String email, String password);

    String generateToken();
    
    User getUserWithToken(String token);
}
