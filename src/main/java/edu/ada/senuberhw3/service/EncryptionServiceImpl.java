package edu.ada.senuberhw3.service;

import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class EncryptionServiceImpl implements EncryptionService
{
	@Override
	public String encrypt(String string)
	{
		String result = "ENCRYPTION ERROR";
		
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			
			byte[] hash = digest.digest(string.getBytes(StandardCharsets.UTF_8));
			
			StringBuilder hexString = new StringBuilder(2 * hash.length);
			
			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
			
			result = hexString.toString();
			
		} catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		
		return result;
	}
}
