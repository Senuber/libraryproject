package edu.ada.senuberhw3.service;

import edu.ada.senuberhw3.model.Book;
import edu.ada.senuberhw3.model.BookModel;
import edu.ada.senuberhw3.model.BookTaking;
import edu.ada.senuberhw3.model.User;
import edu.ada.senuberhw3.repository.BookRepository;
import edu.ada.senuberhw3.repository.BookTakingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService{

    private BookRepository bookRepository;
    private BookTakingRepository bookTakingRepository;
    
    @Autowired
    private CommentService commentService;
    
    @Autowired
    public BookServiceImpl(BookRepository bookRepository, BookTakingRepository bookTakingRepository)
    {
        this.bookRepository = bookRepository;
        this.bookTakingRepository = bookTakingRepository;
        
        if(!bookRepository.findAll().iterator().hasNext()) // adds some new books if there is none
        {
            bookRepository.save(new Book("BOOK1", "Drama", "AUTHOR1", "02.01.2015"));
            bookRepository.save(new Book("BOOK2", "Fiction", "AUTHOR2", "25.05.1925"));
            bookRepository.save(new Book("BOOK3", "Science", "AUTHOR3", "13.04.1850"));
        }
    }
    
    @Override
    public List<Book> getAll()
    {
        Iterable<Book> booksIter = bookRepository.findAll();
        List<Book> books = new ArrayList<>();
        booksIter.forEach(books::add);
        
        return books;
    }
    
    @Override
    public List<Book> getByName(String name) {
        List<Book> books = bookRepository.findAllByName(name);
        return books;
    }
    
    @Override
    public List<Book> getByCategory(String category) {
        List<Book> books = bookRepository.findAllByCategory(category);
        return books;
    }

    @Override
    public List<Book> getByAuthor(String author) {
        List<Book> books = bookRepository.findAllByAuthor(author);
        return books;
    }

    @Override
    public List<Book> getByNameAndCategory(String name, String category) {
        List<Book> books = bookRepository.findAllByNameAndCategory(name, category);
        return books;
    }

    @Override
    public List<Book> getByNameAndAuthor(String name, String author) {
        List<Book> books = bookRepository.findAllByNameAndAuthor(name, author);
        return books;
    }

    @Override
    public List<Book> getByCategoryAndAuthor(String category, String author) {
        List<Book> books = bookRepository.findAllByCategoryAndAuthor(category, author);
        return books;
    }

    @Override
    public List<Book> getByNameAndCategoryAndAuthor(String name, String category, String author) {
        List<Book> books = bookRepository.findAllByNameAndCategoryAndAuthor(name, category, author);
        return books;
    }
    
    @Override
    public Book getById(Long id)
    {
        Optional<Book> book = bookRepository.findById(id);
        return book.orElse(null);
    }
    
    @Override
    public BookModel getBookModelById(Long id)
    {
        Optional<Book> book = bookRepository.findById(id);
        BookModel bookModel = null;
        
        if(book.isPresent())
        {
            bookModel = new BookModel(book.get());
            bookModel.setComments(commentService.getCommentByBookExtId(id));
        }
        
        return bookModel;
    }
    
    @Override
    public boolean takeBook(User user, Book book)
    {
        if(bookTakingRepository.findFirstByBookAndDoneFalse(book) != null) return false; // can't take this book
        
        BookTaking bookTaking = new BookTaking(user, book);
        bookTakingRepository.save(bookTaking);
        
        return true;
    }
    
    @Override
    public boolean returnBook(User user, Book book)
    {
        if(bookTakingRepository.findFirstByBookAndDoneFalse(book) == null) return false; // can't return this book
        
        BookTaking bookTaking = bookTakingRepository.findFirstByBookAndDoneFalse(book);
        bookTaking.setDone(true);
        bookTakingRepository.save(bookTaking);
        
        return true;
    }
}
