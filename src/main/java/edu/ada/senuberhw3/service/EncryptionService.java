package edu.ada.senuberhw3.service;

public interface EncryptionService
{
	String encrypt(String string);
}
