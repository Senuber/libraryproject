package edu.ada.senuberhw3.service;

import edu.ada.senuberhw3.model.Comment;
import edu.ada.senuberhw3.model.CommentModel;
import edu.ada.senuberhw3.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService
{
	private CommentRepository commentRepository;
	
	@Autowired
	public CommentServiceImpl(CommentRepository commentRepository)
	{
		this.commentRepository = commentRepository;
	}
	
	@Override
	public List<CommentModel> getCommentByBookExtId(Long bookID)
	{
		List<CommentModel> commentModels = new ArrayList<>(1);
		Optional<List<Comment>> comment = commentRepository.findAllByBookExtId(Long.toString(bookID));
		if(comment.isEmpty()) return new ArrayList<>(1);
		
		if(comment.get().size() > 0)
		{
			comment.get().stream().forEach(commentEntity -> {
				commentModels.add(new CommentModel(commentEntity));
			});
		}
		
		return commentModels;
	}
	
	@Override
	public void createComment(String commentAuthorName, Long bookID, String commentContent)
	{
		commentRepository.save(new Comment(Long.toString(bookID), commentAuthorName, commentContent));
	}
	
	@Override
	public boolean createReply(String commentAuthorName, String commentID, String commentContent)
	{
		Optional<Comment> comment = commentRepository.findById(commentID);
		if(comment.isEmpty()) return false;
		
		Comment commentEntity = comment.get();
		commentEntity.getReplies().add(new Comment(commentEntity.getBookExtId(), commentEntity.getCommentAuthorName(), commentContent));
		commentRepository.save(commentEntity);
		
		return true;
	}
}
