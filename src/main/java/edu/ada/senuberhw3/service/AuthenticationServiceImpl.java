package edu.ada.senuberhw3.service;

import edu.ada.senuberhw3.controller.AuthenticationWSImpl;
import edu.ada.senuberhw3.model.RegistrationModel;
import edu.ada.senuberhw3.model.User;
import edu.ada.senuberhw3.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private EncryptionService encryptionService;

    @Override
    public User registration(RegistrationModel registrationModel)
    {
        if(userRepository.findFirstByEmail(registrationModel.getEmail()) != null)
        {
            log.info("User existed");
            return null;
        }

        registrationModel.setPassword(encryptionService.encrypt(registrationModel.getPassword()));
        User user = new User(registrationModel);
        user.setToken(this.generateToken());
        userRepository.save(user);
        return user;
    }

    @Override
    public User login(String email, String password) {
        User user;
    
        user = userRepository.findFirstByEmail(email);

        if(user != null && user.getId() > 0)
        {
            user = userRepository.findFirstByEmailAndPassword(email, encryptionService.encrypt(password));

            if(user != null && user.getId() > 0)
            {
                log.info("Login is entered correctly");
            }
            else
            {
                log.info("Password is not correct");
            }
        }
        else
        {
            log.info("User not found");
        }

        return user;
    }

    @Override
    public String generateToken() {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        int length = 15;

        for(int i = 0; i < length; i++) {
            int index = random.nextInt(alphabet.length());
            char randomChar = alphabet.charAt(index);
            sb.append(randomChar);
        }

        return sb.toString();
    }
    
    @Override
    public User getUserWithToken(String token)
    {
        return userRepository.findFirstByToken(token);
    }
}
