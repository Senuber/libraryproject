package edu.ada.senuberhw3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Senuberhw3Application {

    public static void main(String[] args) {
        SpringApplication.run(Senuberhw3Application.class, args);
    }

}
