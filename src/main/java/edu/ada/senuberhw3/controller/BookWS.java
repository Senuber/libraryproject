package edu.ada.senuberhw3.controller;

import org.springframework.http.ResponseEntity;

public interface BookWS {
    ResponseEntity getBookById(int id);
    Object getAll();
    Object getByName(String name);
    Object getByCategory(String category);
    Object getByAuthor(String author);
    Object getByNameAndCategory(String name, String category);
    Object getByNameAndAuthor(String name, String author);
    Object getByCategoryAndAuthor(String category, String author);
    Object getByNameAndCategoryAndAuthor(String name, String category, String author);
    Object takeBook(String token, Long bookID);
    Object returnBook(String token, Long bookID);
    Object getHistory(String token);
    Object getTakenBookList(String token);
    
    //comments
    Object createComment(String token, String commentContent, String id);
    Object createReplyComment(String token, String commentID, String commentContent);
}
