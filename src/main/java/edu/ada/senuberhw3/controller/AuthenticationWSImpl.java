package edu.ada.senuberhw3.controller;

import edu.ada.senuberhw3.model.RegistrationModel;
import edu.ada.senuberhw3.model.User;
import edu.ada.senuberhw3.repository.UserRepository;
import edu.ada.senuberhw3.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthenticationWSImpl implements AuthenticationWS {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    @GetMapping("/login")
    public ResponseEntity login(
            @RequestHeader("email") String email,
            @RequestHeader("password") String password)

    {
        log.info("Email is :: {}", email);
        log.info("Password is :: {}", password);

        User user = authenticationService.login(email, password);

        if(user != null)
        {
            return ResponseEntity.ok("Token is " + user.getToken());
        }

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @Override
    @PostMapping("/register")
    public ResponseEntity register(
            @RequestBody RegistrationModel formData) {
        log.info("Form :: {}", formData);

        User user = authenticationService.registration(formData);
        if(user == null)
        {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity("Token is " + user.getToken(), HttpStatus.CREATED);
    }
}
