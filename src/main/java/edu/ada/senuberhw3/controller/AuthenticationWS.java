package edu.ada.senuberhw3.controller;

import edu.ada.senuberhw3.model.RegistrationModel;
import org.springframework.http.ResponseEntity;

public interface AuthenticationWS {
    ResponseEntity login(String email, String password);

    ResponseEntity register(RegistrationModel formData);
}
