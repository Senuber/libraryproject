package edu.ada.senuberhw3.controller;

import edu.ada.senuberhw3.model.*;
import edu.ada.senuberhw3.service.AuthenticationService;
import edu.ada.senuberhw3.service.BookService;
import edu.ada.senuberhw3.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/book")
public class BookWSImpl implements BookWS {

    @Autowired
    private BookService bookService;
    
    @Autowired
    private AuthenticationService authenticationService;
    
    @Autowired
    private CommentService commentService;
    
    @Override
    @GetMapping("/getAll")
    public Object getAll()
    {
        return bookService.getAll();
    }
    
    @Override
    @GetMapping("/byName")
    public Object getByName(
            @RequestHeader(name = "name") String name) {
        List<Book> book = bookService.getByName(name);

        if(book == null || book.isEmpty())
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return book;
    }

    @Override
    @GetMapping("/byCategory")
    public Object getByCategory(@RequestHeader(name = "category") String category) {
        List<Book> book = bookService.getByCategory(category);

        if(book == null || book.isEmpty())
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return book;
    }

    @Override
    @GetMapping("/byAuthor")
    public Object getByAuthor(@RequestHeader(name = "author") String author) {
        List<Book> book = bookService.getByAuthor(author);

        if(book == null || book.isEmpty())
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return book;
    }

    @Override
    @GetMapping("/byNameAndCategory")
    public Object getByNameAndCategory(
            @RequestHeader(name = "name") String name,
            @RequestHeader(name = "category") String category) {
        List<Book> book = bookService.getByNameAndCategory(name, category);

        if(book == null || book.isEmpty())
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return book;
    }

    @Override
    @GetMapping("/byNameAndAuthor")
    public Object getByNameAndAuthor(
            @RequestHeader(name = "name") String name,
            @RequestHeader(name = "author") String author)
    {
        List<Book> book = bookService.getByNameAndAuthor(name, author);

        if(book == null || book.isEmpty())
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return book;
    }

    @Override
    @GetMapping("/byCategoryAndAuthor")
    public Object getByCategoryAndAuthor(
            @RequestHeader(name = "category") String category,
            @RequestHeader(name = "author") String author) {
        List<Book> book = bookService.getByCategoryAndAuthor(category, author);

        if(book == null || book.isEmpty())
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return book;
    }

    @Override
    @GetMapping("/byNameAndCategoryAndAuthor")
    public Object getByNameAndCategoryAndAuthor(
            @RequestHeader(name = "name") String name,
            @RequestHeader(name = "category") String category,
            @RequestHeader(name = "author") String author) {
        List<Book> book = bookService.getByNameAndCategoryAndAuthor(name, category, author);

        if(book == null || book.isEmpty())
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return book;
    }
    
    @Override
    @PostMapping("/take/{bookID}")
    public Object takeBook(
            @RequestHeader String token,
            @PathVariable Long bookID)
    {
        User user = authenticationService.getUserWithToken(token);
        Book book = bookService.getById(bookID);
        
        if (user == null)
        {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        else if (book == null)
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        
        if (!bookService.takeBook(user, book))
        {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @Override
    @PostMapping("/return/{bookID}")
    public Object returnBook(
            @RequestHeader String token,
            @PathVariable Long bookID)
    {
        User user = authenticationService.getUserWithToken(token);
        Book book = bookService.getById(bookID);
        
        if (user == null)
        {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        else if (book == null)
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    
        if (!bookService.returnBook(user, book))
        {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @Override
    @GetMapping("/history")
    public Object getHistory(
            @RequestHeader String token)
    {
        User user = authenticationService.getUserWithToken(token);
        
        if (user == null)
        {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        
        return user.getHistory().isEmpty() ? ResponseEntity.notFound().build() : user.getHistory();
    }
    
    @Override
    @GetMapping("/bookList")
    public Object getTakenBookList(
            @RequestHeader String token)
    {
        User user = authenticationService.getUserWithToken(token);
    
        if (user == null)
        {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    
        List<BookTaking> currentBooks =  user.getHistory();
        currentBooks.removeIf(BookTaking::isDone);
        
        return currentBooks.isEmpty() ? ResponseEntity.notFound().build() : currentBooks;
    }
    
    @Override
    @GetMapping(value = "/{id}")
    public ResponseEntity getBookById(@PathVariable int id)
    {
        BookModel book = bookService.getBookModelById((long) id);
        return (book == null) ? ResponseEntity.notFound().build() : ResponseEntity.ok(book);
    }
    
    @Override
    @PostMapping(value = "/comment/{id}")
    public Object createComment(
            @RequestHeader String token,
            @RequestParam String commentContent,
            @PathVariable String id)
    {
        User user = authenticationService.getUserWithToken(token);
    
        if (user == null)
        {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        
        commentService.createComment(user.concatFirstAndLastName(), Long.parseLong(id), commentContent);
        
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @Override
    @PostMapping(value = "/reply/{commentID}")
    public Object createReplyComment(
            @RequestHeader String token,
            @PathVariable String commentID,
            @RequestParam String commentContent)
    {
        User user = authenticationService.getUserWithToken(token);
    
        if (user == null)
        {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        
        if (commentService.createReply(user.concatFirstAndLastName(), commentID, commentContent))
            return new ResponseEntity(HttpStatus.OK);
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
